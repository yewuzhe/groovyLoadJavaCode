package cn.zhongqs.groovyLoadJavaCode.glue;

public abstract class Handler {

	public abstract void execute() throws Exception;

	public void init() throws Exception {
		// do something
	}

	public void destroy() throws Exception {
		// do something
	}
}
