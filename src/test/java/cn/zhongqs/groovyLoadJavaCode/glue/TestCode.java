package cn.zhongqs.groovyLoadJavaCode.glue;

import groovy.lang.GroovyClassLoader;
import org.junit.Test;

public class TestCode {

    private GroovyClassLoader groovyClassLoader = new GroovyClassLoader();

    private String code = "package cn.zhongqs.groovyLoadJavaCode.glue;\n" +
            "public class Code extends Handler {\n" +
            "    @Override\n" +
            "    public void execute() throws Exception {\n" +
            "        System.out.println(\"888888\");\n" +
            "    }\n" +
            "}";

    @Test
    public void test1() {
        Class cls = groovyClassLoader.parseClass(code);

        try {
            Handler c = (Handler) cls.newInstance();
            c.execute();
            System.out.println("================test1执行完成===================");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

   @Test
   public void test() {
        try {
            Handler originJobHandler = GlueFactory.getInstance().loadNewInstance(code);
            originJobHandler.execute();
            System.out.println("================test执行完成===================");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}